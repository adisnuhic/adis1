<!DOCTYPE html>
<html>
<head>
	<title>Test 1</title>
	<script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap/css/bootstrap.min.css">
	<script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>


	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<form method="post" action="/adis1/post.php" id="myform">
					<div class="input-group">
						<label for="name">Name:</label>
						<input type="text" class="form-control" name="name" id="name">
					</div>
					<br>
					<div class="input-group">
						<label for="name">Quantity:</label>
						<input type="number" class="form-control" name="quantity" id="quantity">
					</div>	
					<div class="input-group">
						<label for="price">price:</label>
						<input type="price" class="form-control" name="price" id="price">
					</div>	
					<br>
					<input type="submit" name="submit">

				</form>

				<ul id="results">
					
				</ul>

			</div><!-- end col -->
		</div><!-- end row -->
	</div><!-- end container -->


	<script type="text/javascript">
			$(document).ready(function(){
				
				// On form submit
				$("#myform").submit(function(e){
					e.preventDefault();
					var name = $("#name").val();
					var quantity = $("#quantity").val();
					var price = $("#price").val();
					var datetime = new Date();					
					var total = quantity * price;

					$.ajax({
						method: "POST",
						url: "/adis1/post.php",
						data:{ name:name, quantity:quantity, price:price, datetime:datetime, total:total },
						success: function(data){
							var parsed = JSON.parse(data);
							$("#results").append("<li> <b>Name:</b> "+ parsed.name+" <b>Quantity:</b> "+ parsed.quantity+ " <b>Price:</b> "+ parsed.price+" <b>DateTime:</b> "+ parsed.datetime+" <b>Total:</b> "+ parsed.total+"</li>");	
						}
					});

				});
			});



	</script>


</body>
</html>